package com.example.dribble

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.lerp
import androidx.compose.ui.unit.sp
import kotlin.random.Random

const val BAR_WIDTH = 10f
const val GAP_WIDTH = 10f
const val MAX_LINES = 30
const val SECOND = 1000
const val NOT_ANIMATING_DIVIDER = 6f
const val DURATION_MINIMUM = 500
const val DURATION_MAXIMUM = 2000
const val HEIGHT_DIVIDER = 10f
const val REPEAT = 15
const val CORNER_RADIUS = 100f
const val CUTOFF_DIVIDER = 2.5f
const val OVERLAP = 3f
const val TOP_BAR = 5

@Composable
fun FirstPage() {
    var isAnimating by remember { mutableStateOf(false) }
    val heightDivider by animateFloatAsState(
        targetValue = if (isAnimating) 1f else NOT_ANIMATING_DIVIDER,
        animationSpec = tween(SECOND, easing = LinearEasing)
    )
    val infiniteAnimation = rememberInfiniteTransition()
    val animations = mutableListOf<State<Float>>()
    val random = remember { Random(System.currentTimeMillis()) }

    repeat(REPEAT) {
        val duration = random.nextInt(DURATION_MINIMUM, DURATION_MAXIMUM)
        animations += infiniteAnimation.animateFloat(
            initialValue = 0f,
            targetValue = 1f,
            animationSpec = infiniteRepeatable(animation = tween(duration), repeatMode = RepeatMode.Reverse)
        )
    }

    val initialMultipliers = remember {
        mutableListOf<Float>().apply { repeat(MAX_LINES) { this += random.nextFloat() } }
    }

    Canvas(modifier = Modifier.fillMaxSize()) {
        val count = (size.width / (BAR_WIDTH + GAP_WIDTH)).toInt().coerceAtMost(MAX_LINES)
        val animatedVolumeWidth = count * (BAR_WIDTH + GAP_WIDTH)
        var startOffset = (size.width - animatedVolumeWidth) / 2
        val barMinHeight = 0f
        val barMaxHeight = size.height / HEIGHT_DIVIDER / heightDivider
        drawRoundRect(
            color = Color.Magenta,
            size = Size(width = size.width, height = size.height / CUTOFF_DIVIDER),
            cornerRadius = CornerRadius(CORNER_RADIUS, CORNER_RADIUS),
            topLeft = Offset(0f, size.height / OVERLAP)
        )
        drawRect(color = Color.Blue, size = Size(size.width, size.height / TOP_BAR))
        drawRoundRect(
            color = Color.Blue,
            size = Size(width = size.width, height = size.height / 2f),
            cornerRadius = CornerRadius(CORNER_RADIUS, CORNER_RADIUS)
        )
        repeat(count) {
            val currentSize = animations[it % animations.size].value
            var barHeightPercent = initialMultipliers[it] + currentSize
            if (barHeightPercent > 1.0) { barHeightPercent = 2.0f - barHeightPercent }
            val barHeight = lerp(barMinHeight.dp, barMaxHeight.dp, barHeightPercent)
            drawLine(
                color = Color.Green,
                start = Offset(startOffset, size.height / CUTOFF_DIVIDER - barHeight.value / 2),
                end = Offset(startOffset, size.height / CUTOFF_DIVIDER + barHeight.value / 2),
                strokeWidth = BAR_WIDTH,
                cap = StrokeCap.Round
            )
            startOffset += BAR_WIDTH + GAP_WIDTH
        }
    }

    DisplayColumns() {
        isAnimating = !isAnimating
        isAnimating
    }
}

@Composable
fun DisplayColumns(onClick: () -> Boolean) {
    Column() {
        Column(
            modifier = Modifier
                .weight(2f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Aurora Aksnes",
                color = Color.White,
                fontSize = 36.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 64.dp)
            )
            Text(
                text = "Norwegian singer/songwriter AURORA works in similar dark pop mileu as artists like Oh La...",
                color = Color.White,
                fontSize = 19.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(48.dp)
            )
            var buttonText by remember { mutableStateOf("Play") }
            Button(
                onClick = {
                    val play = onClick()
                    buttonText = if (play) "Pause" else "Play"
                }
            ) { Text(text = buttonText) }
        }
    }
}
