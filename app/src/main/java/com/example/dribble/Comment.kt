package com.example.dribble

import java.time.LocalDate

/**
 * Comment dummy data.
 *
 * @property avatarID
 * @property name
 * @property text
 * @property date
 * @constructor Create empty Comment
 */
data class Comment(
    val avatarID: String = "avatar",
    val name: String = "name",
    val text: String = "text",
    val date: LocalDate = LocalDate.parse("1999-12-31")
)
