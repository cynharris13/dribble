package com.example.dribble

import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.dribble.ui.theme.DribbleTheme
import java.time.LocalDate

val commentItemHeight = 50.dp

@Composable
fun SecondPage() {
    val comments = listOf(Comment(), Comment(), Comment(), Comment(), Comment())
    Column(modifier = Modifier.fillMaxHeight()) {
        Canvas(modifier = Modifier.fillMaxWidth().height(400.dp)) {
            drawRect(color = Color.Blue, size = Size(size.width, size.height / TOP_BAR))
            drawRoundRect(
                color = Color.Blue,
                size = Size(width = size.width, height = size.height),
                cornerRadius = CornerRadius(CORNER_RADIUS, CORNER_RADIUS)
            )
        }
        LazyColumn() {
            items(comments) {
                BoxWithConstraints(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(MaterialTheme.colorScheme.surface),
                    contentAlignment = Alignment.Center
                ) {
                    var isActionsVisible by remember { mutableStateOf(false) }
                    val density = LocalDensity.current
                    val maxWidth = with(density) { constraints.maxWidth.toDp() }
                    val maxHeight = commentItemHeight - 16.dp
                    CommentListItem(
                        avatarId = it.avatarID,
                        name = it.name,
                        text = it.text,
                        date = it.date,
                        onClick = { isActionsVisible = true }
                    )
                    ActionPanel(
                        modifier = Modifier
                            .align(Alignment.CenterEnd)
                            .padding(horizontal = 16.dp),
                        maxWidth = maxWidth,
                        maxHeight = maxHeight,
                        isVisible = isActionsVisible,
                        onActionClick = { isActionsVisible = false }
                    )
                }
            }
        }
    }
}

const val CORNER_DURATION = 300
const val EXPAND_DURATION = 300
const val MIN_RADIUS = 10

@Composable
fun ActionPanel(modifier: Modifier, maxWidth: Dp, maxHeight: Dp, isVisible: Boolean, onActionClick: () -> Unit) {
    val stateTransition = updateTransition(targetState = isVisible, label = "Visibility")
    val targetWidth by stateTransition.animateDp(
        transitionSpec = {
            keyframes {
                durationMillis = CORNER_DURATION + EXPAND_DURATION
                if (targetState) {
                    maxHeight at CORNER_DURATION
                    maxWidth at CORNER_DURATION + EXPAND_DURATION
                } else {
                    maxHeight at EXPAND_DURATION
                    MIN_RADIUS.dp at EXPAND_DURATION + CORNER_DURATION
                }
            }
        },
        label = "Width"
    ) { value -> if (value) maxWidth else MIN_RADIUS.dp }

    val targetHeight by stateTransition.animateDp(
        transitionSpec = {
            keyframes {
                durationMillis = CORNER_DURATION + EXPAND_DURATION
                if (targetState) {
                    maxHeight at CORNER_DURATION
                } else {
                    maxHeight at EXPAND_DURATION
                    MIN_RADIUS.dp at EXPAND_DURATION + CORNER_DURATION
                }
            }
        },
        label = "Height"
    ) { value -> if (value) maxHeight else MIN_RADIUS.dp }
    val contentAlphaProvider = remember {
        { (targetWidth - MIN_RADIUS.dp) / (maxWidth - MIN_RADIUS.dp) }
    }
    Row(
        modifier = modifier
            .background(Color.Blue)
            .width(targetWidth)
            .height(targetHeight)
            .alpha(contentAlphaProvider())
    ) {
        Text(text = "➡️", modifier = Modifier.padding(horizontal = padding).clickable { onActionClick() })
        Text(text = "❤️", modifier = Modifier.padding(horizontal = padding))
        Text(text = "⛳️", modifier = Modifier.padding(horizontal = padding))
        Text(text = "🗑", modifier = Modifier.padding(horizontal = padding))
    }
}

val padding = 16.dp

@Composable
fun CommentListItem(
    avatarId: String,
    name: String,
    text: String,
    date: LocalDate,
    onClick: () -> Unit
) {
    Row(modifier = Modifier.clickable { onClick() }.height(commentItemHeight)) {
        Text(text = avatarId, modifier = Modifier.padding(padding))
        Text(text = name, modifier = Modifier.padding(padding))
        Text(text = text, modifier = Modifier.padding(padding))
        Text(text = date.toString(), modifier = Modifier.padding(padding))
    }
}

@Preview(showBackground = true)
@Composable
fun SecondPagePreview() { DribbleTheme { SecondPage() } }
